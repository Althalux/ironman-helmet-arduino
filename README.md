# Ironman Helmet Arduino

arduino code for the Ironman Helmet : https://www.thingiverse.com/thing:4629346

## Getting started

I had to adapt the code because at start-up the servos had too large an amplitude compared to normal operation

if you got an error with the library "servoeasing" use the older version 2.3.4

original project : https://www.thingiverse.com/thing:4629346

