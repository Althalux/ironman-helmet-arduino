#include "ServoEasing.h"
ServoEasing servoTop;
ServoEasing servoBottom;
const int action_pin = 2;
const int ledPin = 6;
const int potPin = A0;
int location = 31;
int bottom_closed = 107;
int bottom_open = 20;
int top_closed = 120;
int top_open = 0;
int value;
int maxBrightness;
void setup()
{
  pinMode(action_pin, INPUT_PULLUP);
  pinMode(potPin, INPUT);
  servoTop.attach(9);
  servoBottom.attach(10);
  setSpeedForAllServos(190);
  servoTop.setEasingType(EASE_CUBIC_IN_OUT);
  servoBottom.setEasingType(EASE_CUBIC_IN_OUT);
}

void loop()
{
  value = analogRead(potPin);
  maxBrightness = map(value, 250, 750, 0, 255);
  int proximity = digitalRead(action_pin);
  if (proximity == LOW)
  {
    if (location > bottom_open)
    {
      servoTop.setEaseTo(top_open);
      servoBottom.setEaseTo(bottom_open);
      synchronizeAllServosStartAndWaitForAllServosToStop();
      location = bottom_open;
      delay(10);
      analogWrite(ledPin, 0);
    }
    else
    {
      servoTop.setEaseTo(top_closed);
      servoBottom.setEaseTo(bottom_closed);
      synchronizeAllServosStartAndWaitForAllServosToStop();
      location = bottom_closed;
      delay(50);
      analogWrite(ledPin, maxBrightness / 3);
      delay(100);
      analogWrite(ledPin, maxBrightness / 5);
      delay(100);
      analogWrite(ledPin, maxBrightness / 2);
      delay(100);
      analogWrite(ledPin, maxBrightness / 3);
      delay(100);
      analogWrite(ledPin, maxBrightness);
      delay(100);
    }
  }
}
